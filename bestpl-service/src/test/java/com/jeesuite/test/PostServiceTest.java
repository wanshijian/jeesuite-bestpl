/**
 * 
 */
package com.jeesuite.test;

import org.apache.commons.lang3.RandomUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jeesuite.bestpl.dto.Post;
import com.jeesuite.bestpl.service.PostService;
import com.jeesuite.spring.InstanceFactory;
import com.jeesuite.spring.SpringInstanceProvider;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:test-root.xml"})
public class PostServiceTest implements ApplicationContextAware{
	
	@Autowired PostService postService;
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Override
	public void setApplicationContext(ApplicationContext arg0) throws BeansException {	
		InstanceFactory.setInstanceProvider(new SpringInstanceProvider(arg0));
	}
	
	@Test
	public void test(){
		String[] titles = new String[]{
				"App Store支持微信支付，去年接入支付宝",
				"休斯顿新闻台记者在飓风哈维报道中拯救了司机的生命",
				"Google Fonts & 前端静态资源公共库 CDN 服务整理",
				"ChirpChange：又一个“总统发推你捐款”项目、每笔最低2美分",
				"西部数据收购云服务公司Upthere",
				"iPhone 7s Plus的PCB板示意图曝光",
				"超级火山将毁灭人类？NASA要冷却火山阻止浩劫"
		};
		
		String[] tags = new String[]{
				"科技,云计算，智能硬件","人类,google,iPhone","iPhone,云计算","支付,推特","CDN,开源项目","云计算，智能硬件","iPhone,支付宝"
		};
		
		for (int i = 0; i < titles.length; i++) {			
			Post post = new Post();
			post.setCategoryId(1000 + RandomUtils.nextInt(0, 6));
			post.setTitle(titles[i]);
			post.setContent(post.getTitle());
			post.setIsOriginal(true);
			post.setIsRecommend(post.getCategoryId() < 1002);
			post.setSummary(post.getTitle());
			post.setUserId(1000);
			post.setUserName("vakinge");
			post.setTags(tags[RandomUtils.nextInt(0, tags.length)]);
			postService.addPosts(post);
		}
	}
	
	
	
	
}
