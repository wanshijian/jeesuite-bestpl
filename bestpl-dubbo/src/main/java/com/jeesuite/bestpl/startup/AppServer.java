/**
 * 
 */
package com.jeesuite.bestpl.startup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.jeesuite.spring.InstanceFactory;
import com.jeesuite.spring.SpringInstanceProvider;

/**
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2016年10月21日
 */
public class AppServer {

	private final static Logger logger = LoggerFactory.getLogger(AppServer.class);

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		//初始化本地日志输出目录
		final ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath*:spring/root.xml");

		logger.info("启动app-server....");

		// 初始化spring 工厂
		InstanceFactory.setInstanceProvider(new SpringInstanceProvider(context));

		System.out.println("==============================");
		System.out.println("|---------服务启动成功----------|");
		System.out.println("==============================");

		// 注册服务关闭钩子
		context.registerShutdownHook();
		Thread.currentThread().join();

	}


}
