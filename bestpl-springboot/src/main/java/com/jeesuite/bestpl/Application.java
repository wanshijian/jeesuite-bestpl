package com.jeesuite.bestpl;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.AsyncEventBus;
import com.jeesuite.bestpl.event.MessageSendEventListener;
import com.jeesuite.common.util.ResourceUtils;
import com.jeesuite.kafka.spring.TopicProducerSpringProvider;
import com.jeesuite.springboot.starter.cache.EnableJeesuiteCache;
import com.jeesuite.springboot.starter.kafka.EnableJeesuiteKafkaProducer;
import com.jeesuite.springboot.starter.mybatis.EnableJeesuiteMybatis;
import com.jeesuite.springboot.starter.scheduler.EnableJeesuiteSchedule;

@SpringBootApplication
@MapperScan(basePackages = "com.jeesuite.bestpl.dao.mapper")
@EnableJeesuiteCache
@EnableJeesuiteMybatis
@EnableJeesuiteSchedule
@EnableJeesuiteKafkaProducer
@EnableRedisHttpSession
@ComponentScan(value = {"com.jeesuite.bestpl"})
@ImportResource(locations = {"dubbo/consumer.xml"})
public class Application {
	public static void main(String[] args) {
		new SpringApplicationBuilder(Application.class).web(true).run(args);
	}
	
	@Bean
    RestTemplate restTemplate() {
		return new RestTemplate();
    }
	
	 @Bean
     public JedisConnectionFactory connectionFactory() {
         JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
         jedisConnectionFactory.setHostName(ResourceUtils.getProperty("session.redis.host"));
         jedisConnectionFactory.setPort(ResourceUtils.getInt("session.redis.port"));
         jedisConnectionFactory.setPassword(ResourceUtils.getProperty("session.redis.password"));
		return jedisConnectionFactory;
     }
	 
//	 @Bean
//	 public StringRedisTemplate stringRedisTemplate(JedisConnectionFactory connectionFactory){
//		 return new StringRedisTemplate(connectionFactory);
//	 }
//	 
//	 @Bean
//	 public RedisTemplate<String,Object> redisTemplate(JedisConnectionFactory connectionFactory){
//		 RedisTemplate<String, Object> template = new RedisTemplate<>();
//		 template.setKeySerializer(new StringRedisSerializer());
//		 template.setHashKeySerializer(new StringRedisSerializer());
//		 return template;
//	 }
	
	@Bean
	AsyncEventBus asyncEventBus(TopicProducerSpringProvider topicProducerProvider){
		ExecutorService executorService = Executors.newFixedThreadPool(3);
		AsyncEventBus eventBus = new AsyncEventBus(executorService);
		//
		eventBus.register(new MessageSendEventListener(topicProducerProvider));
		return eventBus;
	}
  
}
