package com.jeesuite.bestpl.dao.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.jeesuite.bestpl.dao.entity.UserEntity;
import com.jeesuite.mybatis.plugin.cache.annotation.Cache;

import tk.mybatis.mapper.common.BaseMapper;

public interface UserEntityMapper extends BaseMapper<UserEntity> {
	
	@Cache
	@Select("SELECT * FROM sc_user  where username=#{username} limit 1")
	@ResultMap("BaseResultMap")
	UserEntity findByName(@Param("username") String name);
	
	@Cache
	@Select("SELECT * FROM sc_user  where mobile=#{mobile} limit 1")
	@ResultMap("BaseResultMap")
	UserEntity findByMobile(@Param("mobile") String mobile);
	
	@Cache
	@Select("SELECT * FROM sc_user  where email=#{email} limit 1")
	@ResultMap("BaseResultMap")
	UserEntity findByEmail(@Param("email") String email);

}