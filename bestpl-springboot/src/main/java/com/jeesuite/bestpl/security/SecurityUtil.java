package com.jeesuite.bestpl.security;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.jeesuite.bestpl.dto.Constants;
import com.jeesuite.bestpl.exception.JeesuiteBaseException;

public class SecurityUtil {
	
	public static boolean isLogined(){
		return getLoginUserInfo() != null;
	}

	public static LoginUserInfo getLoginUserInfo(){
		 HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		return (LoginUserInfo) request.getSession().getAttribute(Constants.LOGIN_SESSION_KEY);
	}
	
	public static int getLoginUserId(){
		LoginUserInfo userInfo = getLoginUserInfo();
		if(userInfo == null)throw new JeesuiteBaseException(401, "未登录或登录超时");
		return userInfo.getId();
	}
	
}
